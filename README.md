# About
A middleware to sit between a Slack (Slash Command) Plugin and a
Moses SMT server.

Since the Slack Slash Command Plugin sends text only in a certain
format that can not be influenced, and the Moses SMT server only
accepts request in a certain different format, we need to mediate
between the two.
