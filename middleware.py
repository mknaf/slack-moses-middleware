#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import requests
import settings
import time
import xmlrpc.client

from flask import Flask, jsonify, request
from multiprocessing import Process

app = Flask(__name__)


def translate_and_respond(request):
    args = request.form["text"].split()
    srclang = args.pop(0)  # src lang comes first
    trglang = args.pop(0)  # trg lang comes second
    srctext = " ".join(args)  # rest should be the actual text

    # where to send our response to
    response_url = request.form["response_url"]

    # decide on which mosesserver to use
    url = "http://localhost:{}/RPC2".format(settings.langpair_to_port[srclang + trglang])

    # get the translation
    with xmlrpc.client.ServerProxy(url) as moses:
        translation = moses.translate({
            "text": srctext,
            "align": "false",
            "report-all-factors": "false"
        })["text"]

    # use the request body sent by Slack as the response body
    response = request.form.to_dict()
    # but exchange the text with the translation
    response["text"] = "@{} via Translack:".format(response["user_name"])
    response["attachments"] = [{
        "text": translation
    }]
    # make this a verbose message in the channel
    response["response_type"] = "in_channel"

    # Slack expects this in the headers
    headers = {"content-type": "application/json"}

    # send to Slack!
    r = requests.post(
        response_url,
        data=json.dumps(response),
        headers=headers
    )
    return

def help():
    return """
Use the following command format:

`/translack <src lang> <trg lang> <text to translate>`

Example:

`/translack de es Ich bin jetzt im Büro.`

Currently only a de->es model is ready for use.

_Only_ you _can improve /translack. Join #translack today!_
"""


@app.route("/translack", methods=["POST"])
def hello():

    args = request.form["text"].split()

    # if we didn't get srclang and trglang
    if len(args) < 3:
        return help()

    # if someone is asking for help
    if args[0].lower() == "help":
        return help()

    # start a new process that takes care of translating and responding
    p = Process(target=translate_and_respond, args=(request,))
    p.daemon = True
    p.start()

    return "Translacking..."


if __name__ == "__main__":
    app.run()#debug=True)
